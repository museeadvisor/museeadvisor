import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ZXingScannerModule } from '@zxing/ngx-scanner';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Element01Component } from './element01/element01.component';
import { Element02Component } from './element02/element02.component';
import { Element03Component } from './element03/element03.component';
import { SplashComponent } from './splash/splash.component';
import { ScanComponent } from './scan/scan.component';
import { InstructionsComponent } from './instructions/instructions.component';
import { MenuComponent } from './menu/menu.component';
import { SelectionComponent } from './selection/selection.component';
import { QueriesComponent } from './queries/queries.component';
import { PhotoComponent } from './photo/photo.component';

@NgModule({
  declarations: [
    AppComponent,
    Element01Component,
    Element02Component,
    Element03Component,
    SplashComponent,
    ScanComponent,
    InstructionsComponent,
    MenuComponent,
    SelectionComponent,
    QueriesComponent,
    PhotoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ZXingScannerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
