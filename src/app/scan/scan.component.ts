import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'mao-scan',
  templateUrl: './scan.component.html',
  styleUrls: ['./scan.component.scss']
})
export class ScanComponent implements OnInit {

  public qrResultString: string | undefined = undefined;

  constructor(public route: ActivatedRoute, public router: Router) { }

  ngOnInit(): void {
  }

}
