import { Component } from '@angular/core';

@Component({
  selector: 'mao-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'MuseoAdvisOr';
}
