import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Element01Component } from './element01/element01.component';
import { Element02Component } from './element02/element02.component';
import { Element03Component } from './element03/element03.component';
import { SplashComponent } from './splash/splash.component';
import { ScanComponent } from './scan/scan.component';
import { InstructionsComponent } from './instructions/instructions.component';
import { MenuComponent } from './menu/menu.component';
import { SelectionComponent } from './selection/selection.component';
import { QueriesComponent } from './queries/queries.component';
import { PhotoComponent } from './photo/photo.component';

const routes: Routes = [
  { path: '', redirectTo: 'splash', pathMatch: 'full' },
  { path: 'splash', component: SplashComponent },
  { path: 'instructions', component: InstructionsComponent },
  { path: 'scan', component: ScanComponent },
  { path: 'element/1', component: Element01Component },
  { path: 'element/2', component: Element02Component },
  { path: 'element/3', component: Element03Component },
  { path: 'menu', component: MenuComponent },
  { path: 'selection', component: SelectionComponent },
  { path: 'queries', component: QueriesComponent },
  { path: 'photo', component: PhotoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
